/** 
Input:
    so:24

Các bước xử lý:
-Tạo biến lưu giá trị so
-tạo biên lưu giá trị soDvi,soChuc,Tong
-soDvi = so%10
-soChuc = so/10
-Tong = soDvi + soChuc

Output:
    Tong = ?
*/
var so = 24;
var soDvi = null;
var soChuc = null;
soChuc = Math.floor(so / 10);
soDvi = so % 10;
var Tong = null;
Tong = soChuc + soDvi;
console.log("Tong", Tong);
