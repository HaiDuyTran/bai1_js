/**
Input:
    Lương 1 ngày:100.000
    Số ngày làm:30

Các bước xử lý:
-Tạo 2 biến lưu giá trị lương 1 ngày và số ngày làm
-Tạo biến lưu giá trị tiền lương
-Sử dụng toán tử *

Out put
    tienLuong = ?
 */
var luong1ngay = 100000;
var soNgayLam = 30;
var tienLuong = null;
tienLuong = luong1ngay * soNgayLam;
console.log("tienLuong", tienLuong);


