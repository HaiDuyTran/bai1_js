/** 
Input:
    giaUSD:23.500
    soTien:2

Các bước xử lý:
-Tạo 2 biến lưu giá trị giaUSD và soTienQuyDoi
-Tạo biến lưu giá trị tiền sau khi quy đổi
-Sử dụng toán tử *

Output:
    tienQuyDoi = ?
*/
var giaUSD = 23500;
var soTien = 2;
var soTienQuyDoi = null;
soTienQuyDoi = giaUSD * soTien;
console.log("soTienQuyDoi", soTienQuyDoi);
