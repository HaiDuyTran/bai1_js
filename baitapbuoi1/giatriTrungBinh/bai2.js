/**
 Input:
    Giá trị 5 số thực

Các bước xử lý:
-Tạo ra 5 biến số thực
-Tạo biến lưu giá trị trung bình
-Công thức:Tổng 5 số thực chia 5

Output:
    soTb = ?

 */
var so1 = 1;
var so2 = 2;
var so3 = 3;
var so4 = 4;
var so5 = 5;
var soTb = null;
soTb = (so1 + so2 + so3 + so4 + so5) / 5;
console.log("soTb", soTb);
