/** 
Input:
    cDai:5cm
    cRong:4cm

Các bước xử lý:
-Tạo 2 biến lưu giá trị cDai và cRong hcn
-Tạo 2 biến lưu giá trị chu vi và diện tích hcn
-Công thức tính chu vi:(cDai+cRong)*2
-Công thức tính diện tích:cDai*cRong

Output:
    chuVi = ?
    dienTich = ?
*/
var cDai = 5;
var cRong = 4;
var chuVi = null;
var dienTich = null;
chuVi = (cDai + cRong) * 2;
dienTich = cDai * cRong;
console.log("chuVi", chuVi);
console.log("dienTich", dienTich);
